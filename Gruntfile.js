/*eslint-env node*/
/*global module:false*/
module.exports = function (grunt) {

    grunt.initConfig({
        dir: {
            webapp: "src",
            tests: "test",
            dist: "dist",
            localServerTestUrl: "http://localhost:8080/test-resources"
        },

        tests: {
            opaTimeout: 900000
        },

        connect: {
            options: {
                port: 8080,
                hostname: "*"
            },

            serve: {
                options: {
                    open: {
                        target: "http://localhost:8080/index.html"
                    }
                }
            },

            src: {},

            dist: {
                options: {
                    open: {
                        target: "http://localhost:8080/build.html",
						app: 'Google Chrome'
                    }
                }
            }
        },

        openui5_connect: {
            serve: {
                options: {
                    appresources: ["."],
                    testresources: ["<%= dir.tests %>"]
                }
            },

            src: {
                options: {
                    appresources: ["."],
                    testresources: ["<%= dir.tests %>"]
                }
            },
            dist: {
                options: {
                    appresources: ".",
                    testresources: ["<%= dir.tests %>"]
                }
            }
        },

        clean: {
            dist: "<%= dir.dist %>/"
        },

        copy: {
            dist: {
                files: [{
                    expand: true,
                    cwd: "<%= dir.webapp %>",
                    src: [
                        "**",
                        "!test/**"
                    ],
                    dest: "<%= dir.dist %>"
                }]
            }
        },

        eslint: {
            options: {
                quiet: true
            },

            all: ["<%= dir.tests %>", "<%= dir.webapp %>"],
            webapp: ["<%= dir.webapp %>"]
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks("grunt-contrib-connect");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks("grunt-text-replace");
    grunt.loadNpmTasks("grunt-openui5");
    grunt.loadNpmTasks("grunt-eslint");
    grunt.loadNpmTasks("grunt-contrib-qunit");
	grunt.loadNpmTasks('grunt-changelog');

    // Server task
    grunt.registerTask("serve", function () {
        grunt.task.run("openui5_connect:serve" + ":keepalive");
    });

    // Linting task
    grunt.registerTask("lint", ["eslint:all"]);

	// Generate Chanelog
	grunt.registerTask("changelog", ["changelog"]);

    // Build task
    grunt.registerTask("build", ["clean", "openui5_preload", "copy", "replace", "changelog"]);
    grunt.registerTask("buildRun", ["build", "serve:dist"]);

    // Test task
    grunt.registerTask("test", ["openui5_connect:src", "qunit:unit", "qunit:opa"]);
    grunt.registerTask("unitTest", ["openui5_connect:src", "qunit:unit"]);
    grunt.registerTask("opaTest", ["openui5_connect:src", "qunit:opa"]);



    // Default task
    grunt.registerTask("default", [
        "lint:all",
        "test"
    ]);
};
